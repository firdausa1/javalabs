<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Orhanerday\OpenAi\OpenAi;

class ChatGPT
{
    protected $open_ai_key;
    protected $open_ai;

    public function __construct()
    {
        $this->open_ai_key = getenv('OPENAI_API_KEY');
        $this->open_ai = new OpenAi($this->open_ai_key);
    }

    public function getResponse($query)
    {
        $chat = $this->open_ai->chat([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                [
                    "role" => "user",
                    "content" => $query
                ]
            ],
            'temperature' => 1.0,
            'max_tokens' => 4000,
            'frequency_penalty' => 0,
            'presence_penalty' => 0,
         ]);

         $d = json_decode($chat);
         $r = str_replace("\n","<br>", $d->choices[0]->message->content);
         return $r;
    }
}