<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Config;

class SendWA
{
    public function send($telp, $msg)
    {
        $data = [
            'api_key' => 'OBdReS6fPnxaJhLXbnyo8SvEQo3fGV',
            'sender' => '6285231863026',
            'number' => $telp,
            'message' => $msg
        ];
        $curl = curl_init();                              
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://wa.srv32.wapanels.com/send-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
                                                
        $response = curl_exec($curl);
                                            
        curl_close($curl);
        return $response;

    }

    public function random_otp()
    {
        return rand(1000,9999);
    }
}