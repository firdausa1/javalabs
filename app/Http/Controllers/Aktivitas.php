<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\Models\User;
use App\Models\DetBelajar;

class Aktivitas extends Controller
{
    
    public function index()
    {
        if(Auth::check()) {
            $aktivitas = DetBelajar::where('det_belajar.id_user', Auth::id())
                                ->select('t_materi.id', 't_materi.judul', 't_materi.deskripsi', 'det_belajar.status', 'det_belajar.created_at', 't_materi.img')
                                ->join('t_materi', 't_materi.id', 'det_belajar.id_materi')
                                ->orderBy('det_belajar.id_materi', 'asc')
                                ->get();

            return view('aktivitas', ['aktivitas' => $aktivitas]);
        } else {
            return view('login');
        }
    }
    

}
