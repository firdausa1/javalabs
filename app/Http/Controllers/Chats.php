<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\Models\User;
use App\Models\Chat;
use App\Helpers\ChatGPT;

class Chats extends Controller
{
    public function index(){
        if(Auth::check()) {
            return view('chat');
        } else {
            return redirect()->route('login', []);
        }
    }


    public function chat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'prompt'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'msg' => $validator->errors(),
            ]);
		}

        try {
            if(@$request->id_materi && @$request->id_materi != NULL){
                $id_materi = $request->id_materi;
            } else {
                $id_materi = NULL;
            }
            $data_chat_user= array(
                'id_materi' => $id_materi,
                'id_user' => auth()->user()->id,
                'chat' => $request->prompt,
                'role' => 'user',
            );
            $insert_chat_user = Chat::create($data_chat_user);

            $query = $request->prompt;
            $chatgpt = new ChatGPT();
            $response = $chatgpt->getResponse($query);

            if($response){
                //insert ke DB
                $data_chat_ai = array(
                    'id_materi' => $request->id_materi,
                    'id_user' => auth()->user()->id,
                    'chat' => $response,
                    'role' => 'ai',
                );
                $insert_chat_ai = Chat::create($data_chat_ai);

                return response()->json([
                    'msg'       =>  'sukses',
                    'status'    => 1,
                    'user'      => $request->prompt,
                    'ai'        => $response,
                    'created_at_user'=>  date('d-m-Y H:i', strtotime($insert_chat_user->created_at)),
                    'created_at_ai'=> date('d-m-Y H:i', strtotime($insert_chat_user->created_at)),
                ]);
            } else {
                return response()->json([
                    'msg'=> 'AI gagal mendapatkan informasi.',
                    'status'=>0
                ]);
            }

        } catch (Exception $e) {
            return response()->json([
                'msg'=>$e,
                'status'=>0
            ]);
        }

        
    }

    public function get_chat(Request $request)
    {
        try {

            $data = Chat::where('id_materi', $request->id_materi)
                        ->where('id_user', auth()->user()->id)
                        ->orderBy('created_at', 'ASC')
                        ->get();

            return response()->json([
                'status' => 1,
                'data' => $data
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status' => 0,
                'msg' => 'Data tidak ditemukan.',
            ]);
        }
    }
    

}
