<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\Models\User;
use App\Models\Materi;

class Home extends Controller
{
    public function index()
    {
        if(Auth::check()) {

            $materi = Materi::get();

            return view('home', ['materi' => $materi]);
        } else {
            return redirect()->route('login', []);
        }
    }

}
