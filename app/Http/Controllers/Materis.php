<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\Models\User;
use App\Models\Materi;
use App\Models\DetBelajar;
use App\Models\Tes;
use App\Models\Opsi;

class Materis extends Controller
{
    
    public function index($id)
    {
        if(Auth::check()) {
            //insert progress belajar
            $data = array(
                'id_user' => Auth::id(),
                'id_materi' => $id,
            );
            //cek duplikasi
            $cek = DetBelajar::where('id_user', Auth::id())
                                ->where('id_materi', $id)
                                ->first();
            
            if($cek == NULL){
                DetBelajar::create($data);
            }
            $materi = Materi::where('id', $id)->first();

            //SOAL latihan
            //select soal dan jawaban
            $soal = Tes::where('id_materi', $id)->get();
            $list_soal = array();
            foreach($soal as $s){
                //get opsi
                $opsi = Opsi::where('id_tes', $s->id)->select('opsi', 'status')->inRandomOrder()->get()->toArray();
                $soal = array(
                    'soal' => $s->soal,
                    'opsi' => (array) $opsi
                );
                array_push($list_soal, $soal);
            }

            return view('materi', ['materi' => $materi, 'soal' => $list_soal]);
        } else {
            return view('login');
        }
    }

    public function submit_tes($id){
        $data = DetBelajar::where('id_materi', $id)
                            ->where('id_user', Auth::id())
                            ->first();
        $data->status = true;
        $data->save();

        return response()->json([
            'msg' => 'Tes tersimpan. Silahkan periksa jawaban anda.',
            'status' => 1
        ]);
    }

}
