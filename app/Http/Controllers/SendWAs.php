<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\Helpers\SendWA;

class SendWAs extends Controller
{
    protected $send_wa;
    
    public function __construct()
    {
        $this->send_wa = new SendWA();
    }

    public function wa(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'telp'=>'required|numeric',
            'msg'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'msg' => $validator->errors(),
            ]);
		}

        try {

            $this->send_wa->send($request->telp, $request->msg);

        } catch (Exception $e) {
            return response()->json([
                'msg'=>$e,
                'status'=>0
            ]);
        }
    }
    

}
