<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\Models\User;
use App\Helpers\SendWA;

class Users extends Controller
{
    protected $send_wa;
    
    public function __construct()
    {
        $this->send_wa = new SendWA();
    }
    
    public function index()
    {
        if(Auth::check()) {
            return redirect()->route('home', []);
        } else {
            return view('hero');
        }
    }

    public function hero()
    {
        return view('hero');
    }

    public function login()
    {
        if(Auth::check()) {
            return redirect()->route('home', []);
        } else {
            return view('login');
        }
    }

    public function register()
    {
        return view('register');
    }

    public function otp()
    {
        return view('otp');
    }

    public function otp_verif()
    {
        return view('otp_verif');
    }

    public function otp_verif_action(Request $request)
    {
        $otp = $request->num_1.$request->num_2.$request->num_3.$request->num_4;
        $check = User::where('otp', $otp)->first();

        if($check != NULL){ //jika OTP benar
            $credentials = [
                'no_hp' => $check->no_hp,
                'otp' => $otp,
                'password' => $check->no_hp
            ];

            if (Auth::Attempt($credentials)) {
                //update OTP di DB
                $otp_user = User::where('no_hp', $check->no_hp)->first();
                $otp_user->otp = NULL;
                $otp_user->save();

                $data['status'] = "1";
                $data['msg'] = "Login berhasil!";
            } else {
                $data['status'] = "0";
                $data['msg'] = "OTP salah1!";
            }

        } else {
            $data['status'] = "0";
            $data['msg'] = "OTP salah2!";
        }

        return response()->json($data);
    }

    public function action_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_hp'=>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'msg' => $validator->errors(),
            ]);
		}
        
        //send OTP
        //get OTP
        $otp = $this->send_wa->random_otp();
        $msg = '*'.$otp.'* is your verification code. For your security, do not share this code.';
        if ($this->send_wa->send($request->no_hp, $msg)){
            //update OTP di DB
            $otp_user = User::where('no_hp', $request->no_hp)->first();

            if($otp_user != NULL){
                $otp_user->otp = $otp;
                $otp_user->save();

                $data['status'] = "1";
                $data['msg'] = "OTP Sent!";
            } else {
                $data['status'] = "0";
                $data['msg'] = "No whatsapp dan password salah!";
            }
            
        } else {
            $data['status'] = "0";
            $data['msg'] = "No whatsapp dan password salah!";
        }

        return response()->json($data);
    }
    
    public function action_register(Request $request) {
        $validator = Validator::make($request->all(), [
            'nama'=>'required',
            'no_hp'=>'required|unique:t_users|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'msg' => $validator->errors(),
            ]);
		}

        try {
            //send OTP
            //get OTP
            $otp = $this->send_wa->random_otp();
            $msg = '*'.$otp.'* is your verification code. For your security, do not share this code.';
            if ($this->send_wa->send($request->no_hp, $msg)){

                $credentials = [
                    'nama' => $request->nama,
                    'no_hp' => $request->no_hp,
                    'role' => 'siswa',
                    'otp' => $otp,
                    'password' => Hash::make($request->no_hp)
                ];
    
                User::insert($credentials);
    
                return response()->json([
                    'msg'=>'Registrasi berhasil',
                    'status'=>1
                ]);

            } else {
                $data['status'] = 0;
                $data['msg'] = "No whatsapp dan password salah!";
            }

        } catch (Exception $e) {
            return response()->json([
                'msg'=>$e,
                'status'=>0
            ]);
        }
    }

    public function edit(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'nama'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'msg' => $validator->errors(),
            ]);
		}

        try {
            $data = User::where('id', $id)->first();
            if($request->get('password')==""){
                $data->nama = $request->nama;
            } else {
                $data->nama = $request->nama;
                $data->password = $request->password;
            }
            $data->save();

            return response()->json([
                'msg' => 'Data berhasil diubah',
                'status' => 1,
                'notif' => 'success',
                'icon' => 'icon-checkmark3'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'msg'=>$e,
                'status'=>0,
                'notif'=>'danger',
                'icon'=>'icon-cross2'
            ]);
        }
    }

    public function get_by_id($id)
    {   
        try {
            $data = User::where('id', $id)->get();

            return response()->json([
                'status' => 1,
                'data' => $data
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status' => 0,
                'msg' => 'Data tidak ditemukan.',
            ]);
        }
    }
    public function get_profile()
    {   
        try {
            $data = User::where('id', auth()->user()->id)->get();

            return response()->json([
                'status' => 1,
                'data' => $data
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status' => 0,
                'msg' => 'Data tidak ditemukan.',
            ]);
        }
    }
    
    function changepassword(Request $request) {
        $validator = Validator::make($request->all(), [
                'password_lama'=>[
                    'required', function ($attribute, $value, $fail) {
                        if (!Hash::check($value, auth()->user()->password)) {
                            $fail('Password lama tidak cocok');
                        }
                    },
                ],
                'password_baru'=>'required_with:password_confirmation|same:password_confirmation|min:8|different:password',
            ],
        );

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'msg' => $validator->errors(),
            ]);
		}
        $passlama = $request->password_lama;
        $passbaru = $request->password_baru;
        try{
            $user = User::findOrFail(auth()->user()->id);
            if (Hash::check($passlama, $user->password)) { 
                $user->fill([
                    'password' => Hash::make($passbaru)
                ])->save();
                return response()->json([
                    'status' => true,
                    'msg' => 'berhasil disimpan',
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'msg' => 'Password lama tidak cocok',
                ]);
            }
        } catch(Exception $e){
            return response()->json([
                'status' => false,
                'msg' => $e,
            ]);
        }
        
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');  
    }

}
