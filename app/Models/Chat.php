<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;
    protected $table = 't_chat';

    protected $fillable = [
        'id', 
        'id_user', 
        'id_materi', 
        'chat',
        'role',
        'created_at', 
        'updated_at'
    ];
}
