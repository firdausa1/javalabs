<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetBelajar extends Model
{
    use HasFactory;
    protected $table = 'det_belajar';

    protected $fillable = [
        'id', 
        'id_user', 
        'id_materi', 
        'status',
        'created_at', 
        'updated_at'
    ];
}
