<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    use HasFactory;
    protected $table = 't_materi';

    protected $fillable = [
        'id', 
        'judul', 
        'deskripsi', 
        'materi', 
        'img', 
        'prompt',
        'icon',
        'created_at', 
        'updated_at'
    ];
}
