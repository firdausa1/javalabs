<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opsi extends Model
{
    use HasFactory;
    protected $table = 't_opsi_tes';

    protected $fillable = [
        'id', 
        'id_tes', 
        'opsi', 
        'status', 
        'created_at', 
        'updated_at'
    ];
}
