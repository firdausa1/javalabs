<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tes extends Model
{
    use HasFactory;
    protected $table = 't_tes';

    protected $fillable = [
        'id', 
        'id_materi', 
        'soal', 
        'created_at', 
        'updated_at'
    ];
}
