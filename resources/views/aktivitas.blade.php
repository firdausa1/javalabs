<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="JavaLabs - Belajar Java Lebih Mudah!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#0134d4">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title>JavaLabs - Belajar Java Lebih Mudah!</title>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/core-img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('assets/img/icons/icon-96x96.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/img/icons/icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('assets/img/icons/icon-167x167.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/icons/icon-180x180.png')}}">
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/tiny-slider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/baguetteBox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rangeslider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vanilla-dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/apexcharts.css')}}">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Web App Manifest -->
    <link rel="manifest" href="{{asset('assets/manifest.json')}}">
  </head>
  <body>
    <!-- Preloader -->
    <div id="preloader">
      <div class="spinner-grow text-primary" role="status"><span class="visually-hidden">Loading...</span></div>
    </div>
    <!-- Internet Connection Status -->
    <!-- # This code for showing internet connection status -->
    <div class="internet-connection-status" id="internetStatus"></div>
    <!-- Header Area -->
    <div class="header-area" id="headerArea">
      <div class="container">
        <!-- Header Content-->
        <div class="header-content header-style-four position-relative d-flex align-items-center justify-content-between">
          <!-- Back Button-->
          <div class="back-button"><a href="/home"><i class="bi bi-arrow-left-short"></i></a></div>
          <!-- Page Title-->
          <div class="page-heading">
            <h6 class="mb-0">Aktivitas</h6>
          </div>
          <!-- User Profile-->
          <div class="user-profile-wrapper"><a class="user-profile-trigger-btn" href="#"><img src="{{asset('assets/img/bg-img/20.jpg')}}" alt=""></a></div>
        </div>
      </div>
    </div>
    <div class="page-content-wrapper py-3" id="chat-wrapper">
      <div class="container">
        <?php
          foreach($aktivitas as $a){
            echo '
              <div class="card">
                <div class="card-body d-flex align-items-center direction-rtl">
                  <div class="card-img-wrap"><img src="https://w7.pngwing.com/pngs/769/163/png-transparent-professional-java-programmer-software-development-software-developer-others-miscellaneous-text-logo.png" alt=""></div>
                  <div class="card-content">
                    <h5 class="mb-1">'.$a->judul.'</h5>
                    <small>'.$a->deskripsi.'</small><br>
                    '.(($a->status == true)?"<span class='badge bg-success'>Sudah Tes</span>":"<span class='badge bg-danger'>Belum Tes</span>").'
                    <br>
                    <a class="btn btn-outline-primary mt-3" href="'.env('APP_URL').'/materi/'.$a->id.'">Lanjutkan</a>
                  </div>
                </div>
              </div>
              <div class="mb-3"></div>
            ';
          }
        ?>
    </div>

    <!-- All JavaScript Files -->
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/slideToggle.min.js')}}"></script>
    <script src="{{asset('assets/js/internet-status.js')}}"></script>
    <script src="{{asset('assets/js/tiny-slider.js')}}"></script>
    <script src="{{asset('assets/js/baguetteBox.min.js')}}"></script>
    <script src="{{asset('assets/js/countdown.js')}}"></script>
    <script src="{{asset('assets/js/rangeslider.min.js')}}"></script>
    <script src="{{asset('assets/js/vanilla-dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/index.js')}}"></script>
    <script src="{{asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/js/dark-rtl.js')}}"></script>
    <script src="{{asset('assets/js/active.js')}}"></script>
    <!-- PWA -->
    <script src="{{asset('assets/js/pwa.js')}}"></script>
  </body>
</html>