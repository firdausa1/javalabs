<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="JavaLabs - Belajar Java Lebih Mudah!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#0134d4">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title>JavaLabs - Belajar Java Lebih Mudah!</title>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/core-img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('assets/img/icons/icon-96x96.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/img/icons/icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('assets/img/icons/icon-167x167.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/icons/icon-180x180.png')}}">
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/tiny-slider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/baguetteBox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rangeslider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vanilla-dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/apexcharts.css')}}">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Web App Manifest -->
    <link rel="manifest" href="{{asset('assets/manifest.json')}}">
  </head>
  <body>
    <!-- Preloader -->
    <div id="preloader">
      <div class="spinner-grow text-primary" role="status"><span class="visually-hidden">Loading...</span></div>
    </div>
    <!-- Internet Connection Status -->
    <!-- # This code for showing internet connection status -->
    <div class="internet-connection-status" id="internetStatus"></div>
    <!-- Header Area -->
    <div class="header-area" id="headerArea">
      <div class="container">
        <!-- Header Content -->
        <div class="header-content position-relative d-flex align-items-center justify-content-between">
          <!-- Chat User Info -->
          <div class="chat-user--info d-flex align-items-center">
            <!-- Back Button -->
            <div class="back-button"><a href="/home"><i class="bi bi-arrow-left-short"></i></a></div>
            <!-- User Thumbnail & Name -->
            <div class="user-thumbnail-name"><img src="{{asset('assets/img/bg-img/2.jpg')}}" alt="">
              <div class="info ms-1">
                <p>Tanya AI</p><span class="active-status">Online</span>
                <!-- span.offline-status.text-muted Last actived 27m ago-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-content-wrapper py-3" id="chat-wrapper">
      <div class="container">
        <div class="chat-content-wrap">
          <!-- Single Chat Item -->
          <div class="single-chat-item">
            <!-- User Avatar -->
            <div class="user-avatar mt-1">
              <!-- If the user avatar isn't available, will visible the first letter of the username.--><span class="name-first-letter">A</span><img src="{{asset('assets/img/bg-img/2.jpg')}}" alt="">
            </div>
            <!-- User Message -->
            <div class="user-message">
              <div class="message-content">
                <div class="single-message">
                  <p>Hai, tanyain aja apa yang kamu belum mengerti di pemrograman java!</p>
                </div>
              </div>
              <!-- Time and Status -->
              <div class="message-time-status">
                <div class="sent-time">Baru Saja</div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    <div class="chat-footer">
      <div class="container h-100">
        <div class="chat-footer-content h-100 d-flex align-items-center">
          <form action="#" method="post" id="form_chat">
            <!-- Message -->
            @csrf
            <input class="form-control" name="prompt" id="prompt" type="text" placeholder="Tulis pertanyaan..." class="dropup me-5">
            
            <!-- Send -->
            <button class="btn btn-submit ms-2" type="submit" id="btn_send">
                <svg class="bi bi-cursor" xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"></path>
                </svg>
            </button>
          </form>
        </div>
      </div>
    </div>
    
    <!-- All JavaScript Files -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/internet-status.js')}}"></script>
    <script src="{{asset('assets/js/dark-rtl.js')}}"></script>
    <!-- Password Strenght -->
    <script src="{{asset('assets/js/pswmeter.js')}}"></script>
    <script src="{{asset('assets/js/active.js')}}"></script>
    <!-- PWA -->
    <script src="{{asset('assets/js/pwa.js')}}"></script>
    <script>
      $("#form_chat").submit(function(event){
            event.preventDefault();
            var data = $("#form_chat").serialize();
            $.ajax({
                type: "POST",
                data: data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{env('APP_URL')}}/chat",
                beforeSend: function() {
                    $('#loading_chat').remove();
                    var list_chat = '<div class="single-chat-item outgoing">'+
                                    '<div class="user-avatar mt-1">'+
                                    '<span class="name-first-letter">A</span><img src="{{asset('assets/img/bg-img/user3.png')}}" alt="">'+
                                    '</div>'+
                                    '<div class="user-message">'+
                                    '<div class="message-content">'+
                                        '<div class="single-message">'+
                                        '<p>'+$("#prompt").val()+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="message-time-status">'+
                                        '<div class="sent-time">Baru saja</div>'+
                                        '<div class="sent-status seen"><i class="bi bi-check-lg" aria-hidden="true"></i></div>'+
                                    '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="single-chat-item" id="loading_chat">'+
                                '<div class="user-avatar mt-1">'+
                                '<span class="name-first-letter">A</span><img src="{{asset('assets/img/bg-img/2.jpg')}}" alt="">'+
                                '</div>'+
                                '<div class="user-message">'+
                                '<div class="message-content">'+
                                    '<div class="single-message">'+
                                    '<div class="typing"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                            '</div>';
                    $(".chat-content-wrap").append(list_chat).fadeIn(4000);
                    $("#prompt").val("");
                },
                success: function(r) {
                    $('#loading_chat').remove();
                    if(r.status === 1){
                        var chat_ = r.ai;
                        chat_.replace("\n", "<br>");
                        list_chat = '<div class="single-chat-item">'+
                                     '<div class="user-avatar mt-1">'+
                                     '<span class="name-first-letter">A</span><img src="{{asset('assets/img/bg-img/2.jpg')}}" alt="">'+
                                     '</div>'+
                                     '<div class="user-message">'+
                                     '<div class="message-content">'+
                                        '<div class="single-message">'+
                                        '<p>'+chat_+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="message-time-status">'+
                                        '<div class="sent-time">'+r.created_at_ai+'</div>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>';
                        $(".chat-content-wrap").append(list_chat).fadeIn(4000);
                        //get_chat();
                    } else {
                        alert('Terjadi kesalahan. Silahkan cek koneksi internet anda!');
                    }
                }
            });
      });

      function get_chat(){
          $.ajax({
              type: "POST",
              data: { id_materi: null},
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              url: "{{env('APP_URL')}}/get_chat",
              success: function(r) {
                  var chats = r.data;
                  var list_chat = "";
                  $.each(chats, (index, item) => {
                      var chat_ = item.chat;
                      chat_.replace("\n", "<br>");
                      if(item.role === 'ai'){
                          list_chat += '<div class="single-chat-item">'+
                                    '<div class="user-avatar mt-1">'+
                                    '<span class="name-first-letter">A</span><img src="{{asset('assets/img/bg-img/2.jpg')}}" alt="">'+
                                    '</div>'+
                                    '<div class="user-message">'+
                                    '<div class="message-content">'+
                                      '<div class="single-message">'+
                                      '<p>'+chat_+'</p>'+
                                      '</div>'+
                                  '</div>'+
                                  '<div class="message-time-status">'+
                                      '<div class="sent-time">'+item.created_at+'</div>'+
                                  '</div>'+
                                  '</div>'+
                              '</div>';
                      } else {
                          list_chat += '<div class="single-chat-item outgoing">'+
                                  '<div class="user-avatar mt-1">'+
                                  '<span class="name-first-letter">A</span><img src="{{asset('assets/img/bg-img/user3.png')}}" alt="">'+
                                  '</div>'+
                                  '<div class="user-message">'+
                                  '<div class="message-content">'+
                                      '<div class="single-message">'+
                                      '<p>'+chat_+'</p>'+
                                      '</div>'+
                                  '</div>'+
                                  '<div class="message-time-status">'+
                                      '<div class="sent-time">'+item.created_at+'</div>'+
                                      '<div class="sent-status seen"><i class="bi bi-check-lg" aria-hidden="true"></i></div>'+
                                  '</div>'+
                                  '</div>'+
                              '</div>';
                      }

                      $(".chat-content-wrap").html(list_chat).fadeIn(4000);

                  });
              }
          });
      }
      get_chat();
    </script>
  </body>
</html>