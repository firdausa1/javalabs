@extends('layout')

@section('section')
<!-- Welcome Toast -->
<div class="toast toast-autohide custom-toast-1 toast-success home-page-toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="7000" data-bs-autohide="true">
    <div class="toast-body">
        <svg class="bi bi-bookmark-check text-white" width="30" height="30" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"></path>
        <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"></path>
        </svg>
        <div class="toast-text ms-3 me-2">
        <p class="mb-1 text-white">Halo, {{auth()->user() != null ?auth()->user()->nama:'User'}}!</p><small class="d-block">Selamat belajar.</small>
        </div>
    </div>
    <button class="btn btn-close btn-close-white position-absolute p-1" type="button" data-bs-dismiss="toast" aria-label="Close"></button>
</div>

    <!-- Tiny Slider One Wrapper -->
    <div class="tiny-slider-one-wrapper">
    <div class="tiny-slider-one">
        <!-- Single Hero Slide -->
        <div>
        <div class="single-hero-slide" style="background-image: url('{{asset('assets/img/slider/1.jpg')}}')">
            <div class="h-100 d-flex align-items-center text-center">
            <div class="container">
                <h3 class="text-white mb-1">Pengenalan Java</h3>
                <p class="text-white mb-4">Yuk, kenalan dulu dengan bahasa java!.</p><a class="btn btn-creative btn-warning" href="#">Detail</a>
            </div>
            </div>
        </div>
        </div>
        <!-- Single Hero Slide -->
        <div>
        <div class="single-hero-slide" style="background-image: url('{{asset('assets/img/slider/1.jpg')}}')">
            <div class="h-100 d-flex align-items-center text-center">
            <div class="container">
                <h3 class="text-white mb-1">Konsep Pemrograman Java</h3>
                <p class="text-white mb-4">Kamu harus tau konsep pemrograman dengan Java itu gimana.</p><a class="btn btn-creative btn-warning" href="#">Detail</a>
            </div>
            </div>
        </div>
        </div>
        <!-- Single Hero Slide -->
        <div> 
        <div class="single-hero-slide" style="background-image: url('{{asset('assets/img/slider/1.jpg')}}')">
            <div class="h-100 d-flex align-items-center text-center">
            <div class="container">
                <h3 class="text-white mb-1">Struktur &amp; Aturan Pada Java</h3>
                <p class="text-white mb-4">Memulai belajar java dengan mencari tau struktur dan aturannya.</p><a class="btn btn-creative btn-warning" href="#">Detail</a>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <div class="pt-3"></div>
    <div class="container direction-rtl">
        <div class="card mb-3">
            <div class="card-body">
                <h3>Materi</h3>
                <div class="pb-3"></div>

                <?php
                    $awal = 1;
                    $count = 1;
                    foreach($materi as $m){
                        if(($count % 3) == 1){
                            if($awal == 1){
                                echo '<div class="row g-3">';
                            }
                        }

                        
                        echo '
                            <div class="col-4">
                                <div class="feature-card mx-auto text-center">
                                    <a href="'.env('APP_URL').'/materi/'.$m->id.'">
                                        <div class="card mx-auto bg-gray"><i class="'.$m->icon.'"></i></div>
                                    </a>
                                    <p class="mb-0">#'.$count.': '.$m->judul.'</p>
                                </div>
                            </div>
                        ';
                        

                        if($awal == 3){
                            echo '</div><div class="pb-4"></div>';
                            $awal = 1;
                        } else {
                            $awal++;
                        }

                        $count++;
                    }
                ?>
            
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="card bg-primary mb-3 bg-img" style="background-image: url('{{asset('assets/img/core-img/1.png')}}')">
            <div class="card-body direction-rtl p-5">
            <h2 class="text-white">Butuh bantuan?</h2>
            <p class="mb-4 text-white">Yuk, tanya aja ke AI biar kamu lebih paham &amp; belajarmu lebih menyenangkan.</p>
            <a class="btn btn-warning" href="/chat">Tanya AI</a>
            </div>
        </div>
    </div>
@endsection