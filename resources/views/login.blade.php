<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="JavaLabs - Belajar Java Lebih Mudah!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#0134d4">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title>Login - Javalabs!</title>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/core-img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('assets/img/icons/icon-96x96.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/img/icons/icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('assets/img/icons/icon-167x167.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/icons/icon-180x180.png')}}">
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/tiny-slider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/baguetteBox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rangeslider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vanilla-dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/apexcharts.css')}}">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Web App Manifest -->
    <link rel="manifest" href="{{asset('assets/manifest.json')}}">
  </head>
  <body>
    <!-- Preloader -->
    <div id="preloader">
      <div class="spinner-grow text-primary" role="status"><span class="visually-hidden">Loading...</span></div>
    </div>
    <!-- Internet Connection Status -->
    <!-- # This code for showing internet connection status -->
    <div class="internet-connection-status" id="internetStatus"></div>
    <!-- Back Button -->
    <div class="login-back-button"><a href="#">
        <svg class="bi bi-arrow-left-short" width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"></path>
        </svg></a></div>
    <!-- Login Wrapper Area -->
    <div class="login-wrapper d-flex align-items-center justify-content-center">
      <div class="custom-container">
        <div class="text-center px-4"><img width="250px" class="login-intro-img" src="{{asset('assets/img/core-img/logo2.png')}}" alt=""></div>
        <!-- Register Form -->
        <div class="register-form mt-4">
          <h6 class="mb-3 text-center">Log in untuk akses pembelajaran.</h6>
          <div id="notif"></div>
          <form action="#" method="post" id="form_login">
            @csrf
            <div class="form-group">
              <input class="form-control" type="text" name="no_hp" id="no_hp" placeholder="No. Whatsapp">
            </div>
            <button class="btn btn-primary w-100" id="btn_login" type="submit">Login</button>
          </form>
        </div>
        <!-- Login Meta -->
        <br>
        <div class="login-meta-data text-center">
          <p class="mb-0">Belum punya akun? <a class="stretched-link" href="/register">Daftar Sekarang</a></p>
        </div>
      </div>
    </div>
    <!-- All JavaScript Files -->
    @yield('script')
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/internet-status.js')}}"></script>
    <script src="{{asset('assets/js/dark-rtl.js')}}"></script>
    <!-- Password Strenght -->
    <script src="{{asset('assets/js/pswmeter.js')}}"></script>
    <script src="{{asset('assets/js/active.js')}}"></script>
    <!-- PWA -->
    <script src="{{asset('assets/js/pwa.js')}}"></script>
    <script type="text/javascript">
        $("#btn_login").click(function(event){
            event.preventDefault();
            var data = $('#form_login').serialize();
            $.ajax({
                type: "POST",
                data: data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{env('APP_URL')}}/login",
                beforeSend: function(){
                  $("#notif").html('<div class="alert alert-warning">Sedang memeriksa...</div>');
                },
                success: function(r) {
                    if (r.status == 0) {
                      $("#notif").html('<div class="alert alert-danger">Login gagal!.</div>');
                    } else {
                      $("#notif").html('<div class="alert alert-success">Login berhasil!.</div>');
                      window.location.assign('{{env('APP_URL')}}/otp_verif?no='+$("#no_hp").val()); 
                    }
                }
            });
        })
    </script>
  </body>
</html>