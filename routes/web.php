<?php
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: *');
// header("Access-Control-Allow-Headers: *");
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\Home;
use App\Http\Controllers\Chats;
use App\Http\Controllers\Materis;
use App\Http\Controllers\Aktivitas;
use App\Http\Controllers\SendWAs;

Route::get('/', [Users::class, 'hero'])->name('hero');

Route::get('/login', [Users::class, 'login'])->name('login');
Route::post('/login', [Users::class, 'action_login']);

Route::get('/register', [Users::class, 'register'])->name('register');
Route::post('/register', [Users::class, 'action_register']);
Route::get('/otp', [Users::class, 'otp'])->name('otp');
Route::get('/otp_verif', [Users::class, 'otp_verif'])->name('otp_verif');
Route::post('/otp_verif', [Users::class, 'otp_verif_action'])->name('otp_verif_action');
Route::get('/logout', [Users::class, 'logout'])->name('logout');

Route::post('/send_message', [SendWAs::class, 'wa'])->name('wa_otp');

Route::group(['middleware' => ['role:siswa']],function () {
    Route::get('/home', [Home::class, 'index'])->name('home');
    Route::get('/materi/{id}', [Materis::class, 'index'])->name('materi');
    Route::post('/materi/submit_tes/{id}', [Materis::class, 'submit_tes'])->name('tes');
    Route::get('/aktivitas', [Aktivitas::class, 'index'])->name('aktivitas');

    Route::get('/chat', [Chats::class, 'index']);
    Route::post('/get_chat', [Chats::class, 'get_chat']);
    Route::post('/chat', [Chats::class, 'chat'])->name('chat');

    
});